# KissCPC

## What is it ?
It's a Distribution of CPCDOS that aims at simplicity of use.
It keeps everything at a minimum. Which makes it a distribution for everyone

## What is CPCDOS ?
It's a kernel, made for making operating system without compiling and all
the headache of making an operating system.

You can have more infos at their [site web](https://cpcdos.net/en)

## How to use it ?
You have to make first a USB key with freedos on it. 
Then, grab the lastest release of CPCDOS. Extract the USB folder on the root
of your USB. Clone the repo in the OS directory (USB:\CPCDOS\OS)

ENJOY :)
